# KIM-API Deb-packages
## Links:

* [LP, openkim-team](https://launchpad.net/~openkim)
* [LP, openkim project](https://launchpad.net/openkim)
* [LP, git import upstream]( https://code.launchpad.net/~openkim/openkim/+git/kim-api-upstream)
* [LP, git import debian]( https://code.launchpad.net/~openkim/+recipe/kim-api )
* [PPA-stable + LAMMPS](https://launchpad.net/~openkim-team/+archive/ubuntu/stable)
* [PPA-daily](https://launchpad.net/~openkim-team/+archive/ubuntu/daily)
* [Recipe for the daily package](https://code.launchpad.net/~openkim-team/+recipe/openkim-daily)
* [DEB-package git-repo](https://gitlab.com/openkim/kim-api-debian)
* [DEPRECATED] [LP, git for daily deb-packages]( https://code.launchpad.net/~openkim-team/openkim/+git/openkim-debian)

## Install a stable package:
```
sudo add-apt-repository ppa:openkim-team/stable
sudo apt-get update
sudo apt-get install libkim-api-v2-dev
```
This repo contains kim-api-v2 and LAMMPS+kim-api-v2 for:
  1. Ubuntu Xenial 16.04
  1. Ubuntu Bionic 18.04
  1. Ubuntu Cosmic 18.10

(manual packages update)

## Install a daily package:
```
sudo add-apt-repository ppa:openkim-team/daily
sudo apt-get update
sudo apt-get install libkim-api-v2-dev
```

This repo contains kim-api-v2 for:
  1. Ubuntu Xenial 16.04
  1. Ubuntu Bionic 18.04
  1. Ubuntu Cosmic 18.10

(automatic packages update)

## Binary packages

* libkim-api-v2-dev:
  * libkim-api-v2.so -> symlink to libkim-api-v2.so.2
  * libkim-api-v2.pc
  * /usr/lib/libexec...
  * /usr/bin/kim-api-v2*
  * bash-completion
  * FindKIM-API-V2.cmake
  * /etc/profile.d/kim*
* libkim-api-v2-2:
  * libkim-api-v2.so.2
* libkim-api-v2-fortran:
  * /usr/include/kim-api-v2/*.mod
  * /usr/include/kim-api-v2/*.fd
* libkim-api-v2-headers:
  * /usr/include/kim-api-v2/*.h
  * /usr/include/kim-api-v2/*.hpp
  * /usr/include/kim-api-v2/*.inc
* libkim-api-v2-models:
  * /usr/lib/*/kim-api-v2/model-drivers
  * /usr/lib/*/kim-api-v2/models
* libkim-api-v2-doc:
  * docs
  * examples

### Dependencies between packages
![Dependencies between packages](packages.png "Dependencies between packages")
